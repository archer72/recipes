

Chocolate Chip Peanut Butter Pancakes

Yields: 7-8 pancakes

Thick peanut butter pancakes loaded with chocolate chips and drizzled with melted, warm peanut butter.

Ingredients

- .1 cup (140 grams or 5 ounces) all-purpose flour
- 2 teaspoons baking powder
- ¼ teaspoon salt
- 2 tablespoons granulated sugar
- 1 large egg
- 1 cup milk
- ¼ cup peanut butter
- ⅓ cup chocolate chips
- canola oil, for spraying the pan
- Peanut butter, melted (to drizzle)

Instructions

1. In a large bowl sift together flour, baking powder and salt. Add sugar. Set aside.
2. In a separate medium bowl, whisk together the egg, milk and peanut butter.
3. Make a well in the dry ingredients and pour in the wet ingredients. Stir until combined and moistened. Don't overmix. Fold in chocolate chips.
4. Heat a griddle or a skillet over medium heat. Coat with cooking spray (or oil/butter). For each pancake, drop ¼ cup of batter onto skillet. Cook 1-2 minutes, until surface of pancakes have some bubbles. Flip carefully and cook 1-2 minutes more. Transfer to a plate and cover loosely with aluminum foil to keep warm. Make sure to coat the skillet with cooking spray before every pancake or batch of pancakes so they don't stick.
5. Serve immediately while pancakes are still warm. Drizzle with melted peanut butter and top with chocolate chips, if desired.


