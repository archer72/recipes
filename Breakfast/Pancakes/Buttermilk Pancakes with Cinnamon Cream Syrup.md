

###Buttermilk Pancakes with Cinnamon Cream Syrup

**Printed from COOKS.COM       ** https://cooks.com/5d21d3k5

- 1 c. flour
- 1 tbsp. sugar
- 2 tsp. baking powder
- 1/2 tsp. soda
- 1/2 tsp. salt

Add: 1 c. buttermilk

Cinnamon Cream Syrup:

- 1 c. sugar
- 1/2 c. light corn syrup, (Karo)
- 1/4 c. water
- 1/2 to 3/4 tsp. cinnamon
- 1/2 c. evaporated milk

In a small saucepan, combine sugar, corn syrup, water, and cinnamon. Bring to boiling over medium heat, stirring constantly. Cook and stir 2 minutes more. Remove from heat and cool 5 minutes, stir in evaporated milk.


