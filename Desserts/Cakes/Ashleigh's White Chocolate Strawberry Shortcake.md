

##Ashleigh's White Chocolate Strawberry Shortcake

**Printed from COOKS.COM      **    https://cooks.com/9n4221vw

1 yellow box cake mix and ingredients to make the cake
2 small instant white chocolate pudding mix
1 8 oz. package Cool Whip (thawed)
1 1/2 cups milk
2-3 containers fresh strawberries
1 cup granulated white sugar
1 white chocolate bar (optional)

Following the instructions on the cake mix, prepare the cake in (2) 8 or 9 inch pans. While the cake is baking in the oven, prepare the strawberries. Rinse the strawberries in cold water, then cut each strawberry into quarters. Cover all the strawberries with white sugar. Place strawberries in a container and refrigerate them.

When cake is finished, allow it to cool, while preparing cream mixture.

Mix instant white chocolate pudding mix in a bowl with the milk (a wire whisk works best). Next fold in the Cool Whip.

In a serving dish, at least 8-9 inches in diameter, and at least 10 inches high, start to layer with cake, then strawberries, then pudding mixture. First cake, then strawberries, then pudding mixture. Refrigerate for about 1 - 2 hours.

Before serving, garnish with grated white chocolate shavings, and sliced strawberries.

