## Blackberry Or Raspberry Jam Cake
**Printed from COOKS.COM**  https://cooks.com/il9ui5py

- 2/3 c. butter
- 1 c. jam (without seeds)
- 1/4 c. buttermilk or sour cream
- 1 tsp. soda
- 1 c. sugar
- 1 tsp. cinnamon
- 3 eggs
- 2 1/2 c. pastry flour
- 1 tsp. vanilla or lemon extract
- Add 1 tsp. each nutmeg and cloves

Cream butter and sugar. Add jam to egg yolks; mix together. Add butter and sugar. Add sifted dry ingredients alternately with buttermilk. Fold in beaten egg whites and 1 teaspoon vanilla; bake in paper lined pans in moderate oven. Use boiled frosting for filling and top of cake.