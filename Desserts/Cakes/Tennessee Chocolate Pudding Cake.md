##Tennessee Chocolate Pudding Cake
**Printed from COOKS.COM https://cooks.com/9x0hc00s**

- 1 1/2 c. white sugar
- 2 c. plain flour
- 4 tsp. baking powder
- 4 tbsp. cocoa
- 1 c. milk
- 6 tbsp. melted butter

Sift sugar, flour, baking powder and cocoa in pan which cake is to be baked (9 x 13). Add milk, butter and vanilla, mix well.

- 1 c. white sugar
- 1 c. brown sugar
- 1/2 c. cocoa

Mix together the above 3 ingredients until well blended. Sprinkle on top of cake. Pour 3 cups cold water on top of batter. Bake in 350 degrees for 45 minutes. Done when pudding is bubbly and thick. Can use 2 cups prepared coffee (hot or cold) and 1 cup cold water. Gives it a richer flavor.