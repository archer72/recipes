
## Blonde Brownies

- Prep Time – 10 minutes
- Cooking Time – 20 minutes
- Makes – 36


### Ingredients

- 2 1/4 cups all-purpose flour
- 2 1/2 teaspoons baking powder
- 1/2 teaspoon salt
- 1 3/4 cups packed brown sugar
- 3/4 cup (1 1/2 sticks) butter or margarine, softened
- 3 large eggs
- 1 teaspoon vanilla extract
- 2 cups (12-ounce package) [NESTLÉ® TOLL HOUSE® Semi-Sweet Chocolate Morsels](https://www.verybestbaking.com/products/4031/tollhouse/nestle-toll-house-semi-sweet-chocolate-morsels/?fromRecipeId=29893)

### Instructions
PREHEAT oven to 350° F. Grease 15 x 10-inch jelly-roll pan.

COMBINE flour, baking powder and salt in small bowl. Beat sugar and butter in large mixer bowl until creamy. Beat in eggs and vanilla extract; gradually beat in flour mixture. Stir in morsels. Spread into prepared pan.

BAKE for 20 to 25 minutes or until top is golden brown. Cool in pan on wire rack.

