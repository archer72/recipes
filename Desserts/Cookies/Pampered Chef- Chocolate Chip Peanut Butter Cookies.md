
### Pampered Chef- Chocolate Chip Peanut Butter Cookies

#### I'm still in search of the perfect recipe for my cookie exchange this weekend.

#### When I made these, I was still working towards my Pampered Chef recipe goal.  I'm doing a great job of stocking up on cookies for the season.  As of now, I have 5 dozen frozen cookies, three different kinds!

#### Again, any cookie recipe that is ready to bake in 10 minutes or less gets a thumbs up from me! These are delicious.  I think next time I'll use creamy peanut butter, there was something about the peanut chunks that I didn't love.  But, I still managed to eat way more than I should have!

## **Chocolate Chip Peanut Butter Cookies**

**Directions:**

1. Preheat oven to 375°F. Measure peanut butter, shortening and brown sugar into Classic Batter Bowl using Measure-All® Cup. Add granulated sugar, egg and vanilla; beat until thoroughly combined.
2. In Small Batter Bowl, stir together flour, baking soda and salt. Add to peanut butter mixture; mix well. Stir in chocolate morsels. Using Medium Scoop, drop batter 2 inches apart ontocookie sheet. Bake 10-15 minutes. Let stand 2 minutes; remove cookies to Cooling Rack to cool.

**Yield:**  4 dozen cookies

