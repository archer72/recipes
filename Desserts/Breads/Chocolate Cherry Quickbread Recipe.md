## Chocolate Cherry Quickbread

- Prep 35 min
- Total 1 hr 30 min
- Servings 10

Here's a favorite we make on rainy weekends. It's something the kids like to help make and it's usually gone by the end of the day. 

By TBSP Geoff

#### Ingredients

- 1 3/4 cups flour
- 1/2 cup unsweetened cocoa powder
- 1 1/2 teaspoons baking powder
- 1/2 teaspoon salt
- 1/4 teaspoon baking soda
- 1 bag (10 ounces) frozen pitted cherries, thawed
- 1 bag (10 ounces) frozen pitted cherries, thawed, drained and coarsely chopped
- 1 cup sugar
- 1/2 cup sour cream 
- 3 large eggs
- 6 tablespoons butter, melted
 
#### Steps

- Preheat oven to 350° F. Grease 9x5 inch loaf pan. In medium bowl, combine flour, cocoa powder, baking powder, salt and baking soda.
- In medium saucepan, combine cherries and sugar. Using a fork (or potato masher if you have one), crush cherries. Cook over medium heat, stirring occasionally for 15 minutes. Transfer to separate bowl and let cool. Add sour cream and eggs. Combine with the flour mixture.
- Fold chopped cherries into batter. Gradually stir in butter. Pour into the load pan. Bake until a toothpick inserted in center comes out clean, about 50-60 minutes. Let cool in pan for 15 minutes, then remove to rack to cool completely.

