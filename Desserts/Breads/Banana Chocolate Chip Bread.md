
Banana Chocolate Chip Bread

**Printed from COOKS.COM** https://cooks.com/ub2c75pj

- 1 c. sugar
- 1 egg
- 1/2 c. butter
- 1 c. mashed, ripe bananas
- 3 tbsp. milk
- 2 c. sifted flour
- 1 tsp. baking powder
- 1/2 tsp. baking soda
- 1 c. chocolate chips
- 1/2 c. finely chopped nuts

Preheat oven to 350 degrees. Cream sugar, egg, and butter until fluffy; set aside. Combine bananas and milk; set aside. Sift flour, baking powder and soda together.

Stir dry ingredients into creamed mixture alternately with banana mixture until just moistened. Stir in chocolate chips and nuts. Grease 9x5x3 loaf pan or 13x9 Pyrex. Bake at 350 degrees for 1 hour. Cool 10 minutes.


