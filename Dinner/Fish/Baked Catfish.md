[Baked Catfish - Recipe - Cooks.com](https://www.cooks.com/rec/doc/prt/0,1917,144190-234203,00.html)

> Baked Catfish
> 
> Printed from COOKS.COMhttps://cooks.com/mn7la1yt
> 
> * * *
> 
> 1 c. dried bread crumbs  
> 3/4 c. grated Parmesan cheese  
> 1/4 c. chopped parsley  
> 1 tsp. paprika  
> 1/2 tsp. oregano  
> 1/4 tsp. basil  
> 2 tsp. salt  
> 1/2 tsp. pepper  
> 1 stick butter
> 
> Preheat oven to 375°F.
> 
> Mix all the above ingredients. Pat fish dry. Dip in melted butter. Roll in crumb mixture.
> 
> Bake 25 minutes or until fish flakes (10 minutes to the inch thickness). You can use any firm fish.

